const pg = require('pg')

const pool = new pg.Pool({
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE_NAME,
  port: process.env.DB_PORT
})

module.exports = pool