const router = require('express').Router()
const indexCtr = require('./../app/controllers/index.controller')

router.get('/', indexCtr.index)
router.get('/pivot', indexCtr.getPivot)

module.exports = router