# FD Engineer Assessment #2

Female Daily engineer assessment test (number 2). The project is about creating SQL and stream it on nodejs web server.

## Getting Started

### Prerequisites


```
node 6.0 or above
npm 5.0 or above
postgres 9.5.12 or above
```

### Database Setup

```
# Create database (postgreSQL)
ex: CREATE DATABASE fd_engineer_assessment_two;

# Restore database scheme & data from .sql file
$ psql -d fd_engineer_assessment_two < [project-dir]/db/database.sql
```

### Setting Up `.env` File
```
# Rename `.env.example` to '.env' and fill it with value
```

### Install Packages
```
$ npm install
```

### Running App
```
$ npm start

# for development
$ npm run dev

access `localhost:3000'
```

## Running the tests
```
npm run test
```

## Built With

* [Express](https://expressjs.com/) - The web framework used
* [Winston](https://github.com/winstonjs) - Used to log http request
* [EJS](http://ejs.co/) - Used for Templating Engine
* [Bootstrap](https://getbootstrap.com/docs/3.3/) - CSS Framework
* [JQuery](https://jquery.com/) - Javascript Library
* [Postgres](https://www.postgresql.org/) - Database

## Authors

* **Raditya Arya** - [Linkedin](https://linkedin.com/in/radityaarya), [GitHub](https://github.com/radityaarya)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details