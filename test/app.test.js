process.env.NODE_ENV = 'test'

const chai = require('chai')
const chaiHttp = require('chai-http')

const should = chai.should()
const expect = chai.expect

const server = require('./../index')
const Person = require('./../app/models/person.model')

chai.use(chaiHttp)

describe('Test response to client', function() {
  describe('Index page (\'/\')', function() {
    it('Should has status code 200', function(done) {
      chai.request(server)
        .get('/')
        .end(function(err, result) {
          result.should.have.status(200)
          result.res.statusMessage.should.equal('OK')
          done()
        })
    })
    it('Response value should not null and not empty', function(done) {
      chai.request(server)
        .get('/')
        .end(function(err, result) {
          result.text.should.not.equal('null')
          result.text.should.not.be.empty
          done()
        })
    })
    it('Response to client should in html', function(done) {
      chai.request(server)
        .get('/')
        .end(function(err, result) {
          expect(result.text).to.match(/<(\"[^\"]*\"|'[^']*'|[^'\">])*>/)
          done()
        })
    })
  })
  
  describe('Pivot page (\'/pivot\')', function() {
    it('Should has status code 200', function(done) {
      chai.request(server)
        .get('/pivot')
        .end(function(err, result) {
          result.should.have.status(200)
          result.res.statusMessage.should.equal('OK')
          done()
        })
    })
    it('Response should value should not null and not empty', function(done) {
      chai.request(server)
        .get('/pivot')
        .end(function(err, result) {
          result.text.should.not.equal('null')
          result.text.should.not.be.empty
          done()
        })
    })
    it('Response to client should in html', function(done) {
      chai.request(server)
        .get('/')
        .end(function(err, result) {
          expect(result.text).to.match(/<(\"[^\"]*\"|'[^']*'|[^'\">])*>/)
          done()
        })
    })
  })
})

describe('Test database & model', function() {
  describe('Table Person (select all)', function() {
    it('Table Person should not null', function(done) {
      Person.findAll(function(err, result) {
        result.should.not.equal('null')
        done()
      })
    })
    it('Table Person should be an array', function(done) {
      Person.findAll(function(err, result) {
        result.should.be.an('array')
        done()
      })
    })
    it('Table Person should not empty', function(done) {
      Person.findAll(function(err, result) {
        result.should.not.be.empty
        done()
      })
    })
    it('First record should equal \'Eddard\'', function(done) {
      Person.findAll(function(err, result) {
        result[0].first_name.toLowerCase().should.equal('eddard')
        done()
      })
    })
  })

  describe('Table Person (get pivot table)', function() {
    it('Result of pivot query should be an array, not null and not empty', function(done) {
      Person.findAllItem((err, resultsItem) => {
        Person.getPivot(resultsItem, (err, result) => {
          result.should.be.an('array')
          result.should.not.equal('null')
          result.should.not.be.empty
          done()
        })
      })
    })
    it('Result of pivot query should contains \'fullname\' & \'email\' data', function(done) {
      Person.findAllItem((err, resultsItem) => {
        Person.getPivot(resultsItem, (err, result) => {
          Object.keys(result[0])[0].should.equal('fullname')
          Object.keys(result[0])[1].should.equal('email')
          done()
        })
      })
    })
  })
})