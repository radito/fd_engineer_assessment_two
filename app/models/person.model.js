const db = require('./../../config/dbConnection')
const table = 'person'

const toSnakeCase = (val) => {
  return val.toLowerCase().split(' ').join('_')
}

const logInfo = (err, query) => {
  if (process.env.NODE_ENV != 'test') {
    if (err) {
      console.error(err)
      return
    }

    console.info("[SQL] EXECUTE =>", query)
  }
}

const dbConnection = (query, callback) => {
  let data = {}

  db.connect((err, client, done) => {
    if (err) {
      logInfo('connection failed')
      return callback(err, null)
    }

    db.query(query, (err, result) => {
      if (err) {
        logInfo('query failed')
        return callback(err, null)
      }

      logInfo(null, query)

      data = result.rows
      done()
      callback(err, data)
    })
  })

  return data
}

module.exports = {
  findAll(callback) {
    const query = `SELECT * FROM ${table}`
  
    return dbConnection(query, callback)
  },
  
  findAllItem(callback) {
    const query = `SELECT item FROM ${table}`
  
    return dbConnection(query, callback)
  },
  
  getPivot(items, callback) {
    const filterQuery =
      items
      .map(m => m.item)
      .filter((v, i, a) => a.indexOf(v) == i)
      .map(m => `COALESCE(SUM(quantity) FILTER(WHERE item='${m}'),0) AS ${toSnakeCase(m)}`)
      .join(', ')
      
    const query =
      `SELECT CONCAT(first_name::varchar, ' ', last_name::varchar) AS fullname, email, ${filterQuery}
      FROM ${table} GROUP BY fullname, email ORDER BY fullname`
      
    return dbConnection(query, callback)
  }
}