const Person = require('./../models/person.model')

module.exports = {
  index(req, res, next) {
    Person.findAll((err, results) => {
      if (err) {
        return next(err)
      }
      
      res.render('index', { results })
    })
  },

  getPivot(req, res, next) {
    Person.findAllItem((err, resultsItem) => {
      if (err) {
        return next(err)
      }
      
      Person.getPivot(resultsItem, (err, resultPivot) => {
        if (err) {
          return next(err)
        }
        
        const items = resultsItem
                      .map(m => m.item.toLowerCase().split(' ').join('_'))
                      .filter((v, i, a) => a.indexOf(v) == i)
                      
        res.render('pivot-table', {results: resultPivot, items})
      })
    })
  }
}