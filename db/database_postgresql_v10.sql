--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: person; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.person (
    id integer NOT NULL,
    first_name character varying,
    last_name character varying,
    email character varying,
    item character varying,
    quantity integer,
    total_price numeric
);


--
-- Name: person_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.person_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.person_id_seq OWNED BY public.person.id;


--
-- Name: person id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person ALTER COLUMN id SET DEFAULT nextval('public.person_id_seq'::regclass);


--
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.person (id, first_name, last_name, email, item, quantity, total_price) FROM stdin;
1	Eddard	Stark	ned@stark.com	Valyrian Steel	2	2000000
2	Sansa	Stark	sansa@stark.com	Coat	3	450000
3	Arya	Stark	arya@stark.com	Needle Sword	1	100000
4	Eddard	Stark	ned@stark.com	Oathkeeper Sword	1	500000
5	Tywin	Lannister	tywin@lannister.com	Valyrian Steel	1	1000000
6	Cersei	Lannister	cersei@lannister.com	Crown	1	500000
7	Arya	Stark	arya@stark.com	Iron Shield	3	300000
8	Tywin	Lannister	tywin@lannister.com	Iron Shield	5	500000
9	Tyrion	Lannister	tyrion@lannister.com	Horse	5	1500000
10	Arya	Stark	arya@stark.com	Horse	10	3000000
11	Cersei	Lannister	cersei@lannister.com	Coat	5	750000
12	Sansa	Stark	sansa@stark.com	Horse	15	4500000
\.


--
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.person_id_seq', 12, true);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

