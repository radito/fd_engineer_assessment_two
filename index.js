require('dotenv').config()
const express = require('express')
const path = require('path')
const morgan = require('morgan')

const app = express()

const logger = require('./config/logger')
const indexRoute = require('./config/routes')

// configure view engine
app.set('views', path.join(__dirname, 'app/views'))
app.set('view engine', 'ejs')

// public
app.use(express.static(path.join(__dirname, 'public')));

// configure logging
if (process.env.NODE_ENV !== 'test') {
  const logFile = process.env.NODE_ENV === 'development'
    ? 'dev'
    : 'combined'
  app.use(morgan(logFile, { stream: logger.stream }))
}

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // winston logging
  logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

// route
app.use('/', indexRoute)

module.exports = app